<?php

declare(strict_types=1);

namespace Infakt\Tests\Client;

use Symfony\Contracts\HttpClient\ResponseInterface;

class TestClient
{
    public function request(string $method, string $url, array $options = []): ResponseInterface
    {
        return new class implements ResponseInterface {
            public function getStatusCode(): int
            {
                return 200;
            }

            public function getHeaders(bool $throw = true): array
            {
                return [];
            }

            public function getContent(bool $throw = true): string
            {
                return '{}';
            }

            public function toArray(bool $throw = true): array
            {
                return [];
            }

            public function cancel(): void
            {
            }

            public function getInfo(string $type = null): mixed
            {
                return null;
            }
        };
    }
}