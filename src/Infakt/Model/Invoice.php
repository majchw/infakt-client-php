<?php

declare(strict_types=1);

namespace Infakt\Model;

use Infakt\Model\Invoice\Extension;
use Infakt\Model\Invoice\Service;

/**
 * This entity represents an invoice.
 *
 * @see https://www.infakt.pl/developers/invoices.html#def
 */
class Invoice implements EntityInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $number;

    /**
     * @var string
     */
    protected $currency;

    /**
     * @var float
     */
    protected $paidPrice;

    /**
     * @var \DateTime
     */
    protected $paidDate;

    /**
     * @var string
     */
    protected $notes;

    /**
     * @var string
     */
    protected $kind;

    /**
     * @var string
     */
    protected $paymentMethod;

    /**
     * @var string
     */
    protected $recipientSignature;

    /**
     * @var string
     */
    protected $sellerSignature;

    /**
     * @var \DateTime
     */
    protected $invoiceDate;

    /**
     * @var \DateTime
     */
    protected $saleDate;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var \DateTime
     */
    protected $paymentDate;

    /**
     * @var float
     */
    protected $netPrice;

    /**
     * @var float
     */
    protected $taxPrice;

    /**
     * @var float
     */
    protected $grossPrice;

    /**
     * @var int
     */
    protected $clientId;

    /**
     * @var string
     */
    protected $clientCompanyName;

    /**
     * @var string
     */
    protected $clientStreet;

    /**
     * @var string
     */
    protected $clientCity;

    /**
     * @var string
     */
    protected $clientPostCode;

    /**
     * @var string
     */
    protected $clientTaxCode;

    /**
     * @var string
     */
    protected $cleanClientNip;

    /**
     * @var string
     */
    protected $clientCountry;

    /**
     * @var string
     */
    protected $bankName;

    /**
     * @var string
     */
    protected $bankAccount;

    /**
     * @var string
     */
    protected $swift;

    /**
     * @var string
     */
    protected $saleType;

    /**
     * @var string
     */
    protected $invoiceDateKind;

    /**
     * @var Service[]
     */
    protected $services;

    /**
     * @var int
     */
    protected $vatExemptionReason;

    /**
     * @var Extension
     */
    protected $extensions;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Invoice
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @return Invoice
     */
    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @return Invoice
     */
    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPaidPrice(): ?float
    {
        return $this->paidPrice;
    }

    /**
     * @return Invoice
     */
    public function setPaidPrice(float $paidPrice): self
    {
        $this->paidPrice = $paidPrice;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getPaidDate(): ?\DateTime
    {
        return $this->paidDate;
    }

    /**
     * @return Invoice
     */
    public function setPaidDate(?\DateTime $paidDate): self
    {
        $this->paidDate = $paidDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    /**
     * @return Invoice
     */
    public function setNotes(?string $notes): self
    {
        $this->notes = $notes ?? '';

        return $this;
    }

    /**
     * @return string|null
     */
    public function getKind(): ?string
    {
        return $this->kind;
    }

    /**
     * @return Invoice
     */
    public function setKind(string $kind): self
    {
        $this->kind = $kind;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    /**
     * @return Invoice
     */
    public function setPaymentMethod(string $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRecipientSignature(): ?string
    {
        return $this->recipientSignature;
    }

    /**
     * @return Invoice
     */
    public function setRecipientSignature(string $recipientSignature): self
    {
        $this->recipientSignature = $recipientSignature;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSellerSignature(): ?string
    {
        return $this->sellerSignature;
    }

    /**
     * @return Invoice
     */
    public function setSellerSignature(string $sellerSignature): self
    {
        $this->sellerSignature = $sellerSignature;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getInvoiceDate(): ?\DateTime
    {
        return $this->invoiceDate;
    }

    /**
     * @return Invoice
     */
    public function setInvoiceDate(\DateTime $invoiceDate): self
    {
        $this->invoiceDate = $invoiceDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSaleDate(): ?\DateTime
    {
        return $this->saleDate;
    }

    /**
     * @return Invoice
     */
    public function setSaleDate(\DateTime $saleDate): self
    {
        $this->saleDate = $saleDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @return Invoice
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getPaymentDate(): ?\DateTime
    {
        return $this->paymentDate;
    }

    /**
     * @return Invoice
     */
    public function setPaymentDate(\DateTime $paymentDate): self
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getNetPrice(): ?float
    {
        return $this->netPrice;
    }

    /**
     * @return Invoice
     */
    public function setNetPrice(float $netPrice): self
    {
        $this->netPrice = $netPrice;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTaxPrice(): ?float
    {
        return $this->taxPrice;
    }

    /**
     * @return Invoice
     */
    public function setTaxPrice(float $taxPrice): self
    {
        $this->taxPrice = $taxPrice;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getGrossPrice(): ?float
    {
        return $this->grossPrice;
    }

    /**
     * @return Invoice
     */
    public function setGrossPrice(float $grossPrice): self
    {
        $this->grossPrice = $grossPrice;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getClientId(): ?int
    {
        return $this->clientId;
    }

    /**
     * @return Invoice
     */
    public function setClientId(int $clientId): self
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getClientCompanyName(): ?string
    {
        return $this->clientCompanyName;
    }

    /**
     * @return Invoice
     */
    public function setClientCompanyName(?string $clientCompanyName): self
    {
        $this->clientCompanyName = $clientCompanyName ?? '';

        return $this;
    }

    /**
     * @return string|null
     */
    public function getClientStreet(): ?string
    {
        return $this->clientStreet;
    }

    /**
     * @return Invoice
     */
    public function setClientStreet(?string $clientStreet): self
    {
        $this->clientStreet = $clientStreet ?? '';

        return $this;
    }

    /**
     * @return string|null
     */
    public function getClientCity(): ?string
    {
        return $this->clientCity;
    }

    /**
     * @return Invoice
     */
    public function setClientCity(?string $clientCity): self
    {
        $this->clientCity = $clientCity ?? '';

        return $this;
    }

    /**
     * @return string|null
     */
    public function getClientPostCode(): ?string
    {
        return $this->clientPostCode;
    }

    /**
     * @return Invoice
     */
    public function setClientPostCode(?string $clientPostCode): self
    {
        $this->clientPostCode = $clientPostCode ?? '';

        return $this;
    }

    /**
     * @return string|null
     */
    public function getClientTaxCode(): ?string
    {
        return $this->clientTaxCode;
    }

    /**
     * @return string|null
     */
    public function getCleanClientNip(): ?string
    {
        return $this->cleanClientNip;
    }

    /**
     * @param string $cleanClientNip
     * @return $this
     */
    public function setCleanClientNip(string $cleanClientNip): self
    {
        $this->cleanClientNip = $cleanClientNip;

        return $this;
    }

    /**
     * @return Invoice
     */
    public function setClientTaxCode(?string $clientTaxCode): self
    {
        $this->clientTaxCode = $clientTaxCode ?? '';

        return $this;
    }

    /**
     * @return string|null
     */
    public function getClientCountry(): ?string
    {
        return $this->clientCountry;
    }

    /**
     * @return Invoice
     */
    public function setClientCountry(string $clientCountry): self
    {
        $this->clientCountry = $clientCountry;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBankName(): ?string
    {
        return $this->bankName;
    }

    /**
     * @return Invoice
     */
    public function setBankName(string $bankName): self
    {
        $this->bankName = $bankName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBankAccount(): ?string
    {
        return $this->bankAccount;
    }

    /**
     * @return Invoice
     */
    public function setBankAccount(string $bankAccount): self
    {
        $this->bankAccount = $bankAccount;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSwift(): ?string
    {
        return $this->swift;
    }

    /**
     * @return Invoice
     */
    public function setSwift(string $swift): self
    {
        $this->swift = $swift;

        return $this;
    }

    /**
     * @return string
     */
    public function getSaleType(): ?string
    {
        return $this->saleType;
    }

    /**
     * @return Invoice
     */
    public function setSaleType(string $saleType): self
    {
        $this->saleType = $saleType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInvoiceDateKind(): ?string
    {
        return $this->invoiceDateKind;
    }

    /**
     * @return Invoice
     */
    public function setInvoiceDateKind(string $invoiceDateKind): self
    {
        $this->invoiceDateKind = $invoiceDateKind;

        return $this;
    }

    /**
     * @return Service[]
     */
    public function getServices(): array
    {
        return $this->services;
    }

    /**
     * @param Service[] $services
     *
     * @return Invoice
     */
    public function setServices(array $services): self
    {
        $this->services = $services;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getVatExemptionReason(): ?int
    {
        return $this->vatExemptionReason;
    }

    /**
     * @return Invoice
     */
    public function setVatExemptionReason(?int $vatExemptionReason): self
    {
        $this->vatExemptionReason = $vatExemptionReason;

        return $this;
    }

    /**
     * @return Extension
     */
    public function getExtensions(): Extension
    {
        return $this->extensions;
    }

    /**
     * @return Invoice
     */
    public function setExtensions(Extension $extensions): self
    {
        $this->extensions = $extensions;

        return $this;
    }

    /**
     * @param array $options
     * @return bool
     */
    public function isValid(array $options = []): bool
    {
        $conditions = [];

        $conditions[] = $this->hasAllRequiredFields($options);

        return !in_array(false, $conditions);
    }

    /**
     * @param array $options
     * @return bool
     */
    private function hasAllRequiredFields(array $options = []): bool
    {
        $conditions = [];

        $conditions[] = !empty($this->getServices());

        if (array_key_exists('read', $options) && $options['read'] === true) {
            $conditions[] = !empty($this->getId());
            $conditions[] = !empty($this->getStatus());
            $conditions[] = !empty($this->getExtensions());
        }

        if (array_key_exists('search', $options) && $options['search'] === true) {
            $conditions[] = !empty($this->getCleanClientNip());
        }

        if (array_key_exists('foreign', $options) && $options['foreign'] === true) {
            $conditions[] = !empty($this->getSaleType());
        }

        return !in_array(false, $conditions);
    }
}
