<?php

declare(strict_types=1);

namespace Infakt\Model;

/**
 * This entity represents a vat rate.
 *
 * @see https://www.infakt.pl/developers/vat_rates.html#def
 */
class VatRate implements EntityInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var float
     */
    protected $rate;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $symbol;

    /**
     * @var string
     */
    protected $validFrom;

    /**
     * @var string
     */
    protected $validUntil;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return VatRate
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    /**
     * @return VatRate
     */
    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return VatRate
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->symbol;
    }

    /**
     * @return VatRate
     */
    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * @return string
     */
    public function getValidFrom(): string
    {
        return $this->validFrom;
    }

    /**
     * @param string $validFrom
     * @return void
     */
    public function setValidFrom(string $validFrom): void
    {
        $this->validFrom = $validFrom;
    }

    /**
     * @return string
     */
    public function getValidUntil(): string
    {
        return $this->validUntil;
    }

    /**
     * @param string $validUntil
     * @return void
     */
    public function setValidUntil(string $validUntil): void
    {
        $this->validUntil = $validUntil;
    }

    /**
     * @param array $options
     * @return bool
     */
    public function isValid(array $options = []): bool
    {
        $conditions = [];

        $conditions[] = $this->hasAllRequiredFields($options);

        return !in_array(false, $conditions);
    }

    /**
     * @param array $options
     * @return bool
     */
    private function hasAllRequiredFields(array $options = []): bool
    {
        $conditions = [];

        if (array_key_exists('read', $options) && $options['read'] === true) {
            $conditions[] = !empty($this->getId());
            $conditions[] = !empty($this->getRate());
            $conditions[] = !empty($this->getName());
            $conditions[] = !empty($this->getSymbol());
            $conditions[] = !empty($this->getValidFrom());
            $conditions[] = !empty($this->getValidUntil());
        }

        return !in_array(false, $conditions);
    }
}
