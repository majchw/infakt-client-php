<?php

declare(strict_types=1);

namespace Infakt\Model;

interface EntityInterface
{
    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @param $id
     *
     * @return $this
     */
    public function setId(int $id);

    /**
     * @param array $options
     * @return bool
     */
    public function isValid(array $options = []): bool;
}