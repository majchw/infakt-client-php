<?php

declare(strict_types=1);

namespace Infakt;

use Symfony\Component\HttpClient\HttpClient;
use Infakt\Exception\LogicException;
use Infakt\Repository\AbstractObjectRepository;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Class Infakt.
 */
class Infakt
{
    const API_ENDPOINT = 'https://api.infakt.pl/api';
    const API_SANDBOX = 'https://api.sandbox-infakt.pl/api';

    const API_VERSION = 'v3';

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var string
     */
    protected $version;

    /**
     * @var HttpClientInterface
     */
    protected $client;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * Infakt constructor.
     */
    public function __construct(string $apiKey, HttpClientInterface $client = null, string $endpoint = null, string $version = null)
    {
        $this->apiKey = $apiKey;
        $this->client = $client ?? new HttpClient();
        $this->endpoint = $endpoint ?? self::API_ENDPOINT;
        $this->version = $version ?? self::API_VERSION;
    }

    /**
     * Return object repository for a specific model class name.
     *
     * @param $className
     *
     * @throws LogicException
     */
    public function getRepository(string $className): AbstractObjectRepository
    {
        $className = 'Infakt\\Repository\\'.\substr($className, \strrpos($className, '\\') + 1).'Repository';

        if (!\class_exists($className)) {
            throw new LogicException("There is no repository to work with class $className.");
        }

        return new $className($this);
    }

    /**
     * Send HTTP GET request.
     */
    public function get(string $query): ResponseInterface
    {
        return $this->request('GET', $query);
    }

    /**
     * Send HTTP DELETE request.
     */
    public function delete(string $query): ResponseInterface
    {
        return $this->request('DELETE', $query);
    }

    /**
     * Send HTTP POST request.
     */
    public function post(string $query, ?string $body = null): ResponseInterface
    {
        return $this->request('POST', $query, $body);
    }

    /**
     * Send HTTP PUT request.
     */
    public function put(string $query, ?string $body = null): ResponseInterface
    {
        return $this->request('PUT', $query, $body);
    }

    /**
     * Attach endpoint URL to the query.
     */
    public function buildQuery(string $query): string
    {
        return $this->endpoint.'/'.$this->version.'/'.$query;
    }

    /**
     * Prepare and perform HTTP request through the client.
     */
    public function request(string $method, string $query, ?string $body = null): ResponseInterface
    {
        $options = [
            'headers' => [
                'X-inFakt-ApiKey' => $this->apiKey,
                'Content-Type: application/json',
                'Accept: application/json'
            ],
        ];

        if ($body) {
            $options['body'] = $body;
        }

        return $this->client->request($method, $this->buildQuery($query), $options);
    }
}
