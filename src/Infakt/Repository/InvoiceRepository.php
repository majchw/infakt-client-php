<?php

declare(strict_types=1);

namespace Infakt\Repository;

use Infakt\Model\Invoice;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class InvoiceRepository extends AbstractObjectRepository
{
    /**
     * Get a next invoice number.
     *
     * @param string $kind
     */
    public function getNextNumber($kind = 'vat'): string
    {
        $kinds = ['final', 'advance', 'margin', 'proforma', 'vat'];

        if (!\in_array($kind, $kinds)) {
            throw new \LogicException('Invalid invoice kind "'.$kind.'"');
        }

        $query = $this->getServiceName().'/next_number.json?kind='.$kind;
        $response = $this->infakt->get($query);

        $data = json_decode($response->getContent(), true);

        return (string) $data['next_number'];
    }

    /**
     * Mark an invoice as paid.
     */
    public function markAsPaid(Invoice $invoice, \DateTime $paidDate = null): ResponseInterface
    {
        $query = $this->getServiceName().'/'.$invoice->getId().'/paid.json';
        return $this->infakt->post($query);
    }

    /**
     * @param Invoice $invoice
     * @param string $printType One of:
     * - original
     * - copy
     * - original_duplicate
     * - copy_duplicate
     * - duplicate
     * - regular
     * @param string|null $locale One of:
     * - pl (Polish)
     * - en (English)
     * - pe (Polish - English)
     * @param string|null $recipient Email of the recipient
     * @param bool|null $sendCopy Send copy to the owner's email account
     * @return ResponseInterface
     * @throws TransportExceptionInterface
     */
    public function deliverViaEmail(
        Invoice $invoice,
        string $printType,
        ?string $locale = null,
        ?string $recipient = null,
        ?bool $sendCopy = null
    ): ResponseInterface
    {
        $body = [
            'print_type' => $printType
        ];

        $optionalParameters = [
            'locale' => $locale,
            'recipient' => $recipient,
            'send_copy' => $sendCopy,
        ];

        foreach ($optionalParameters as $key => $value) {
            if (!is_null($value)) {
                $body[$key] = $value;
            }
        }

        $url = $this->getServiceName().'/'.$invoice->getId().'/deliver_via_email.json';
        return $this->infakt->post($url, json_encode($body));
    }
}
